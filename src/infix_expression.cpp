#include "infix_expression.h"
using namespace Infix;

Object Object::new_operator(Operator o) {
    Object x;
    x.type = Object::Type::Operator;
    x.data.o = o;
    return x;
}
Object Object::new_variable(Variable v) {
    Object x;
    x.type = Object::Type::Variable;
    x.data.v = v;
    return x;
}
Object Object::new_open_bracket() {
    Object x;
    x.type = Object::Type::OpenBracket;
    return x;
}
Object Object::new_close_bracket() {
    Object x;
    x.type = Object::Type::CloseBracket;
    return x;
}
Object Object::new_function(FunctionId f) {
    Object x;
    x.type = Object::Type::Function;
    x.data.f = f;
    return x;
}
const FunctionId& Object::get_function() const {
    return data.f;
}

bool Object::is_function() const { return type == Type::Function; };
bool Object::is_variable() const { return type == Type::Variable; };
bool Object::is_operator() const { return type == Type::Operator; };
bool Object::is_open_bracket() const { return type == Type::OpenBracket; };
bool Object::is_close_bracket() const { return type == Type::CloseBracket; };

bool Object::is_plus() const {
    return is_operator() && data.o == Operator::Plus;
}
bool Object::is_minus() const {
    return is_operator() && data.o == Operator::Minus;
}

Postfix::Object Object::to_posfix_object() const {
    switch (type) {
        case Object::Type::Operator:
            return Postfix::Object::new_operator(data.o);
        case Object::Type::Variable:
            return Postfix::Object::new_variable(data.v);
        case Object::Type::Function:
            return Postfix::Object::new_function(data.f);
        default:
            assert(false);
            throw;
    }
}

int Infix::Object::precedence() const {
    return ::precedence(data.o);
}

namespace Infix {
    std::ostream& operator << (std::ostream& os, const Object &o) {
        if (o.is_open_bracket()) os << "(";
        if (o.is_close_bracket()) os << ")";
        if (o.is_operator()) os << o.data.o;
        if (o.is_variable()) os << o.data.v;
        if (o.is_function()) os << o.data.f->name();
        return os;
    }
}

InfixExpression::InfixExpression(const List<Object> &&l): list(l) {}        

bool InfixExpression::is_valid() const {
    if (list.empty()) return false;
    // Check brackets
    {
        int count = 0;
        for (const Object &o: list) {
            if (o.is_open_bracket()) count++;
            else if (o.is_close_bracket()) count--;
            if (count < 0) return false;
        }
        if (count != 0) return false;
    }

    if (!list.front().is_variable() &&
        !list.front().is_open_bracket() &&
        !list.front().is_function()) {
        return false;
    }

    if (!list.back().is_variable() &&
        !list.back().is_close_bracket()) {
        return false;
    }

    auto next = ++list.begin();
    bool prev_is_const = false;
    for (const Object &o: list) {
        if (next == list.end()) break;
        if (o.is_function()) {
            if (!(*next).is_open_bracket() &&
                !(*next).is_function() &&
                !(*next).is_variable()) {
                return false;
            }
        } else if (o.is_operator()) {
            if (!(*next).is_open_bracket() &&
                !(*next).is_variable() &&
                !(*next).is_function()) {
                return false;
            }
        } else if (o.is_variable()) {
            if (!(*next).is_close_bracket() &&
                !(*next).is_operator()) {
                return false;
            }
        } else if (o.is_open_bracket()) {
            if ((*next).is_close_bracket() && prev_is_const) {}
            else if (!(*next).is_open_bracket() &&
                !(*next).is_variable() &&
                !(*next).is_function()) {
                return false;
            }
        } else if (o.is_close_bracket()) {
            if (!(*next).is_close_bracket() &&
                !(*next).is_operator()) {
                return false;
            }
        }
        prev_is_const = o.is_function() && o.get_function()->get_count() == 0;
        next++;
    }

    return true;
}

using Postfix::PostfixExpression;

PostfixExpression InfixExpression::to_posfix() const {
    List<Postfix::Object> r;
    List<Infix::Object> stack;

    for (const Object& o: list) {
        if (o.is_function()) {
            stack.push_back(o);
        } else if (o.is_variable()) {
            r.push_back(o.to_posfix_object());
        } else if (o.is_open_bracket()) {
            stack.push_back(o);
        } else if (o.is_close_bracket()) {
            while (!stack.back().is_open_bracket()) {
                r.push_back(stack.back().to_posfix_object());
                stack.pop_back();
            }
            stack.pop_back();
            if (!stack.empty() && stack.back().is_function()) {
                r.push_back(stack.back().to_posfix_object());
                stack.pop_back();
            }
        } else if (o.is_operator()) {
            int pre = o.precedence();
            while (!stack.empty()) {
                int prepre = 0;
                if (stack.back().is_function()) prepre = 100;
                else if (stack.back().is_operator()) prepre = stack.back().precedence();
                else break;
                if (prepre < pre) break;
                r.push_back(stack.back().to_posfix_object());
                stack.pop_back();
            }
            stack.push_back(o);
        }
    }
    while (!stack.empty()) {
        r.push_back(stack.back().to_posfix_object());
        stack.pop_back();
    }

    return PostfixExpression(std::move(r));
}
