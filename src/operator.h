#ifndef OPERATOR_H
#define OPERATOR_H
#include <iostream>

enum class Operator {
    Plus,
    Minus,
    Multiply,
    Divide,
    Power,
};
int precedence(Operator o);
std::ostream& operator << (std::ostream&, const Operator &);
#endif
