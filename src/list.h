#ifndef LIST_H
#define LIST_H
#include "exception.h"
#include <assert.h>
template<class T>
class Node {
    T data;
    Node *next, *prev;
    Node(const T &d, Node *p, Node *n): data(d), next(n), prev(p) {}
    template<class U> friend class List;
    template<class U> friend class Iterator;
    template<class U> friend class ConstIterator;
};

template<class T>
class Iterator {
protected:
    Node<T> *ptr;
    Iterator(Node<T> *p): ptr(p) {}
    template<class U> friend class List;
public:
    Iterator next() const {
        assert(ptr != nullptr);
        Iterator r = *this;
        r.ptr = r.ptr->next;
        return r;
    }
    Iterator operator++(int) {
        assert(ptr != nullptr);
        Iterator r = *this;
        ptr = ptr->next;
        return r;
    }
    Iterator& operator++() {
        assert(ptr != nullptr);
        ptr = ptr->next;
        return *this;
    }
    Iterator operator--(int) {
        assert(ptr != nullptr);
        Iterator r = *this;
        ptr = ptr->prev;
        return r;
    }
    Iterator& operator--() {
        assert(ptr != nullptr);
        ptr = ptr->prev;
        return *this;
    }
    T& operator *() {
        assert(ptr != nullptr);
        return ptr->data;
    }
    bool operator == (const Iterator i) const {
        return ptr == i.ptr;
    }
    bool operator != (const Iterator i) const {
        return ptr != i.ptr;
    }
    void insert_after(const T &x) {
        assert(ptr != nullptr);
        assert(ptr->next != nullptr); // append to back is prohibited
        Node<T> *node = new Node<T>(x, ptr, ptr->next);
        assert(node != nullptr);
        node->next->prev = node;
        node->prev->next = node;
    }
    void remove_after() {
        assert(ptr != nullptr);
        assert(ptr->next != nullptr); // there nothing after
        assert(ptr->next->next != nullptr); // remove back is prohibited
        Node<T> *old = ptr->next;
        ptr->next = old->next;
        ptr->next->prev = ptr;
        delete old;
    }
};

template<class T>
class ConstIterator: public Iterator<T> {
    ConstIterator(Node<T> *p): Iterator<T>(p) {}
    template<class U> friend class List;
    public:
    const T& operator *() const {
        return Iterator<T>::ptr->data;
    }
};

template<class T>
class List {
    Node<T> *ptr_front, *ptr_back;
public:
    ~List() {
        for (Node<T> *p = ptr_front; p != nullptr; p = p->next) {
            delete p;
        }
    }

    List(): ptr_front(nullptr), ptr_back(nullptr) {}

    List(const List &l): List() {
        // push all elements of l to this
        for (Node<T> *p = l.ptr_front; p != nullptr; p = p->next) {
            push_back(p->data);
        }
    }

    List& operator = (const List &l) {
        // clear old elements
        for (Node<T> *p = ptr_front; p != nullptr; p = p->next) {
            delete p;
        }
        ptr_front = ptr_back = nullptr;
        // push all elements of l to this
        for (Node<T> *p = l.ptr_front; p != nullptr; p = p->next) {
            push_back(p->data);
        }
    }
    bool empty() const {
        return ptr_front == nullptr;
    }

    const T& back() const {
        assert(!empty());
        return ptr_back->data;
    }

    const T& front() const {
        assert(!empty());
        return ptr_front->data;
    }

    T& back() {
        assert(!empty());
        return ptr_back->data;
    }

    T& front() {
        assert(!empty());
        return ptr_front->data;
    }

    void push_back(const T &t) {
        Node<T> *node = new Node<T>(t, nullptr, nullptr);
        assert(node != nullptr);
        if (empty()) ptr_front = ptr_back = node;
        else {
            node->prev = ptr_back;
            ptr_back->next = node;
            ptr_back = node;
        }
    }

    void pop_back() {
        if (empty()) return;
        Node<T> *prev_back = ptr_back->prev;
        delete ptr_back;
        if (prev_back == nullptr) {
            ptr_front = ptr_back = nullptr;
        } else {
            ptr_back = prev_back;
            ptr_back->next = nullptr;
        }
    }

    void push_front(const T &t) {
        Node<T> *node = new Node<T>(t, nullptr, nullptr);
        assert(node != nullptr);
        if (empty()) ptr_front = ptr_back = node;
        else {
            node->next = ptr_front;
            ptr_front->prev = node;
            ptr_front = node;
        }
    }

    void pop_front() {
        if (empty()) return;
        Node<T> *next_front = ptr_front->next;
        delete ptr_front;
        if (next_front == nullptr) {
            ptr_front = ptr_back = nullptr;
        } else {
            ptr_front = next_front;
            ptr_front->prev = nullptr;
        }
    }

    Iterator<T> begin() {
        return Iterator<T>(ptr_front);
    }
    Iterator<T> end() {
        return Iterator<T>(nullptr);
    }

    ConstIterator<T> end() const {
        return ConstIterator<T>(nullptr);
    }
    ConstIterator<T> begin() const {
        return ConstIterator<T>(ptr_front);
    }
};
#endif
