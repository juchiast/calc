#ifndef CALCULATOR_H
#define CALCULATOR_H
#include "number.h"
#include "map.h"
#include "infix_expression.h"
#include "function.h"
#include <tuple>

class Calculator {
    Number *var[26];
    Function* functions;

    int find_function(int) const;
    std::tuple<Infix::InfixExpression, Map<Number>, char> parse(const char *) const;
    List<Infix::Object> fix(List<Infix::Object> &&) const;
    public:
    Calculator();
    ~Calculator();
    Number evaluate(const char *);
};
#endif
