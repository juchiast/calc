#ifndef FUNCTION_H
#define FUNCTION_H
#include "number.h"
class Function {
    Number (*raw)(Number *);
    int id;
    int count;
    const char *_name;

    static Number _log(Number *);
    static Number _sqrt(Number *);
    static Number _fraction(Number *);
    static Number _floor(Number *);
    static Number _abs(Number *);
    static Number _sin(Number *);
    static Number _cos(Number *);
    static Number _tan(Number *);
    static Number _asin(Number *);
    static Number _acos(Number *);
    static Number _atan(Number *);
    static Number _exp(Number *);
    static Number _inv(Number *);
    static Number _pi(Number *);
    static Number _e(Number *);
    public:
    Function();
    Function(const char *, int, Number (*raw)(Number *));
    Number operator ()(Number *) const;
    static int hash(const char *);
    int get_id() const;
    int get_count() const;
    const char* name() const;


    static Function log, sqrt, fraction, floor, abs, sin, cos, tan, asin, acos, atan, exp, inv, pi, e;
};
#endif
