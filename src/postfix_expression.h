#ifndef POSTFIX_EXPRESSION_H
#define POSTFIX_EXPRESSION_H
#include "list.h"
#include "number.h"
#include "operator.h"
#include "map.h"
#include "function.h"

namespace Postfix {
    typedef int Variable;

    typedef const Function* FunctionId;

    class Object {
        union {
            Variable v;
            Operator o;
            FunctionId f;
        } data;

        public:
        enum class Type {
            Variable,
            Operator,
            Function,
        } type;

        static Object new_variable(Variable);
        static Object new_function(FunctionId);
        static Object new_operator(Operator);
        const Variable& get_variable() const;
        const FunctionId& get_function() const;
        const Operator& get_operator() const;
    };
    class PostfixExpression {
        List<Object> list;
        public:
        PostfixExpression(const List<Object> &&);
        Number evaluate(Map<Number> &&) const;
    };
}
#endif
