#include "function.h"
#include <string.h>
#include <assert.h>

int Function::hash(const char *s) {
    const int B = 1000000007;
    int len = strlen(s);
    long long h=0;
    for (int i=0; i<len; i++) {
        h = (26ll*h + (s[i]-'a')*len) % B;
    }
    return h+len;
}

Function::Function(): raw(nullptr), id(0), count(0) {}

Function::Function(const char *name, int count, Number (*f)(Number *)):
    raw(f), id(hash(name)), count(count), _name(name)
{}

int Function::get_id() const { return id; }
int Function::get_count() const { return count; }
const char* Function::name() const { return _name; }

Number Function::operator()(Number *p) const {
    return raw(p);
}

Function Function::log("log", 1, _log);
Function Function::sqrt("sqrt", 1, _sqrt);
Function Function::fraction("frac", 1, _fraction);
Function Function::floor("floor", 1, _floor);
Function Function::abs("abs", 1, _abs);
Function Function::sin("sin", 1, _sin);
Function Function::cos("cos", 1, _cos);
Function Function::tan("tan", 1, _tan);
Function Function::asin("asin", 1, _asin);
Function Function::acos("acos", 1, _acos);
Function Function::atan("atan", 1, _atan);
Function Function::exp("exp", 1, _exp);
Function Function::inv("inv", 1, _inv);
Function Function::pi("pi", 0, _pi);
Function Function::e("e", 0, _e);

Number Function::_log(Number *p) { return (*p).log(); }
Number Function::_fraction(Number *p) { return (*p).fraction(); }
Number Function::_floor(Number *p) { return (*p).floor(); }
Number Function::_abs(Number *p) { return (*p).abs(); }
Number Function::_sin(Number *p) { return (*p).sin(); }
Number Function::_cos(Number *p) { return (*p).cos(); }
Number Function::_tan(Number *p) { return (*p).tan(); }
Number Function::_asin(Number *p) { return (*p).asin(); }
Number Function::_acos(Number *p) { return (*p).acos(); }
Number Function::_atan(Number *p) { return (*p).atan(); }
Number Function::_exp(Number *p) { return (*p).exp(); }
Number Function::_inv(Number *p) { return -(*p); }
Number Function::_pi(Number *) { return Number("3.1415926535897932384626433832"); }
Number Function::_e(Number *) { return Number("2.7182818284590452353602874713527"); }

Number Function::_sqrt(Number *p) {
    return (*p) ^ Number("0.5");
}
