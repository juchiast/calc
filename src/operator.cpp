#include "operator.h"

int precedence(Operator o) {
    switch(o) {
        case Operator::Plus: return 0;
        case Operator::Minus: return 0;
        case Operator::Multiply: return 1;
        case Operator::Divide: return 1;
        case Operator::Power: return 2;
    }
}

std::ostream& operator << (std::ostream& os, const Operator &o) {
    switch(o) {
        case Operator::Plus: os << "+"; break;
        case Operator::Minus: os << "-"; break;
        case Operator::Multiply: os << "*"; break;
        case Operator::Divide: os << "/"; break;
        case Operator::Power: os << "^"; break;
    }
    return os;
}
