#ifndef MAP_H
#define MAP_H
#include "exception.h"
#include <string.h>
#include <stdlib.h>
#include <assert.h>

// Simple hash map implementation
template<class V>
class Map {
    int size, count;
    int *key;
    V **value;
    void resize() {
        int old_size = size;
        size = size+size/2;
        key = (int*)realloc(key, size*sizeof(int));
        value = (V**)realloc(value, size*sizeof(V*));
        if (key==nullptr || value==nullptr) throw Exception("Fatal: Allocation error");
        for (int i=old_size; i<size; i++) {
            key[i] = 0;
            value[i] = nullptr;
        }
    }
public:
    Map() {
        size = 100;
        count = 0;
        key = (int*)calloc(size, sizeof(int));
        value = (V**)calloc(size, sizeof(V*));
        if (key==nullptr || value==nullptr) throw Exception("Fatal: Allocation error");
    }
    Map(const Map &m) {
        size = m.size;
        count = m.count;
        key = (int*)malloc(size*sizeof(int));
        value = (V**)calloc(size, sizeof(V*));
        if (key==nullptr || value==nullptr) throw Exception("Fatal: Allocation error");
        memcpy(key, m.key, size*sizeof(int));
        for (int i=1; i<size; i++) {
            if (key[i]) value[i] = new V(*(m.value[i]));
        }
    }
    ~Map() {
        for (int i=1; i<size; i++) if (key[i]) delete value[i];
        free(key);
        free(value);
    }
    int insert(const V &val) {
        if ((double)count / size >= 0.7) resize();
        int k;
        do {
            k = rand() % (size-1) + 1;
            while (k < size && key[k]!=0) k++;
        } while (k == size);
        count++;
        key[k] = k;
        value[k] = new V(val);
        return k;
    }
    int insert(V *ptr) {
        if ((double)count / size >= 0.7) resize();
        int k;
        do {
            k = rand() % (size-1) + 1;
            while (k < size && key[k]!=0) k++;
        } while (k == size);
        count++;
        key[k] = k;
        value[k] = ptr;
        return k;
    }
    V& operator [](int k) {
        k = find(k);
        return *(value[k]);
    }
    const V& operator [](int k) const {
        k = find(k);
        return *(value[k]);
    }
    void remove(int k) {
        k = find(k);
        count--;
        key[k] = 0;
        delete value[k];
    }
    int find(int k) const {
        while (k < size && key[k] != k) k++;
        assert(k < size);
        return k;
    }
};
#endif
