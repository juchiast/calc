#include "postfix_expression.h"
using namespace Postfix;

Object Object::new_operator(Operator o) {
    Object x;
    x.type = Object::Type::Operator;
    x.data.o = o;
    return x;
}
Object Object::new_variable(Variable v) {
    Object x;
    x.type = Object::Type::Variable;
    x.data.v = v;
    return x;
}
Object Object::new_function(FunctionId f) {
    Object x;
    x.type = Object::Type::Function;
    x.data.f = f;
    return x;
}
const Variable& Object::get_variable() const {
    return data.v;
}
const Operator& Object::get_operator() const {
    return data.o;
}
const FunctionId& Object::get_function() const {
    return data.f;
}

PostfixExpression::PostfixExpression(const List<Object> &&l): list(l) {}

Number calculate(Operator op, const Number &a, const Number &b) {
    switch (op) {
        case Operator::Plus: return a + b;
        case Operator::Minus: return a - b;
        case Operator::Multiply: return a * b;
        case Operator::Divide: return a / b;
        case Operator::Power: return a ^ b;
    }
}

Number PostfixExpression::evaluate(Map<Number> &&map) const {
    List<Variable> stack;
    for (const Object &o: list) {
        if (o.type == Object::Type::Variable) {
            stack.push_back(o.get_variable());
        } else if (o.type == Object::Type::Operator) {
            Variable s2 = stack.back();
            stack.pop_back();
            Variable s1 = stack.back();
            stack.pop_back();
            Number res = calculate(o.get_operator(), map[s1], map[s2]);
            map.remove(s1);
            map.remove(s2);
            stack.push_back(map.insert(res));
        } else if (o.type == Object::Type::Function) {
            int count = o.get_function()->get_count();
            Number *nums = nullptr;
            if (count > 0) nums = new Number[count];
            for (int i=count-1; i>=0; i--) {
                nums[i] = map[stack.back()];
                stack.pop_back();
            }
            stack.push_back(map.insert((*o.get_function())(nums)));
            delete[] nums;
        }
    }
    return map[stack.front()];
}
