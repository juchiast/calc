#include "src/calculator.h"
#include <sstream>

Calculator *calc = nullptr;

template<class T>
char* to_str(const T& t) {
    std::stringstream ss;
    ss << t;
    std::string s = ss.str();
    char *r = (char*) malloc(strlen(s.data()));
    strcpy(r, s.data());
    return r;
}

extern "C" char* calculate(char *s) {
    if (calc == nullptr) calc = new Calculator();
    try {
        return to_str(calc->evaluate(s));
    } catch (Exception e) {
        return to_str(e);
    }
}

extern "C" void free_mem(char *s) {
    free(s);
}

int main() {}
