emcc: 
	emcc web.cpp src/*.cpp --std=c++11 -O3 -o web/lib.js \
	    -s WASM=1 \
	    -s EXPORTED_FUNCTIONS="['_calculate', '_free_mem']" \
	    -s DISABLE_EXCEPTION_CATCHING=0
